package com.example.listfrutas

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.ListView
import android.widget.Toast

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        var frutas : ArrayList<String> = ArrayList()
        frutas.add("Limon")
        frutas.add("Manzana")
        frutas.add("Naranja")
        frutas.add("Fresa")
        frutas.add("Kiwi")
        frutas.add("Platano")
        frutas.add("Sandia")
        frutas.add("Mango")
        frutas.add("Uva")


        val lista = findViewById<ListView>(R.id.lista)

        val adaptador = ArrayAdapter<String>(this,android.R.layout.simple_list_item_1,frutas)

        lista.adapter= adaptador

        lista.onItemClickListener = AdapterView.OnItemClickListener { adapterView, view, i, l ->
            Toast.makeText(this,frutas.get(i), Toast.LENGTH_LONG).show()

            when(frutas.get(i)) {
                "Manzana" -> startActivity(Intent(this, ManzanaActivity::class.java))
                "Platano" -> startActivity(Intent(this, PlatanoActivity::class.java))
                "Naranja" -> startActivity(Intent(this, NaranjaActivity::class.java))
                "Limon" -> startActivity(Intent(this, LimonActivity::class.java))
                "Sandia" -> startActivity(Intent(this, SandiaActivity::class.java))
                "Mango" -> startActivity(Intent(this, MangoActivity::class.java))
                "Uva" -> startActivity(Intent(this, UvaActivity::class.java))
                "Kiwi" -> startActivity(Intent(this, KiwiActivity::class.java))
                "Fresa" -> startActivity(Intent(this, FresaActivity::class.java))
            }
        }
    }
}