package com.example.listfrutas

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.TextView

class FresaActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_fresa)

        val backHome: TextView = findViewById(R.id.tvFresa)

        backHome.setOnClickListener {
            startActivity(Intent(this,MainActivity::class.java))
        }
    }
}