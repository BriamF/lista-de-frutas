package com.example.listfrutas

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.TextView

class MangoActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_mango)

        val backHome: TextView = findViewById(R.id.tvMango)

        backHome.setOnClickListener {
            startActivity(Intent(this,MainActivity::class.java))
        }
    }
}