package com.example.listfrutas

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.TextView

class LimonActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_limon)
        val backHome: TextView = findViewById(R.id.tvLimon)

        backHome.setOnClickListener {
            startActivity(Intent(this,MainActivity::class.java))
        }
    }

}